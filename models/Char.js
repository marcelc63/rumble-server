var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var ObjectId = mongoose.Schema.Types.ObjectId;
var URLSlugs = require("mongoose-url-slugs");

var Char = new Schema({
  name: String,
  data: String,
  timestamp: Number,
  datestamp: {
    type: Date,
    // `Date.now()` returns the current unix timestamp as a number
    default: Date.now
  }
});

Char.plugin(URLSlugs("name", { update: true }));

module.exports = mongoose.model("Char", Char);
