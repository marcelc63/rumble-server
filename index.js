const express = require("express");
const app = express();
const http = require("http").Server(app);
const port = 3000;
var bodyParser = require("body-parser");

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var mongoose = require("mongoose");
var credentials = require("./config.js");
mongoose.Promise = require("bluebird");
mongoose
  .connect(credentials.mongoConnection, {
    promiseLibrary: require("bluebird"),
    useNewUrlParser: true,
    auth: { authdb: "admin" }
  })
  .then(() => console.log("connection succesful"))
  .catch(err => console.error(err));
mongoose.set("useCreateIndex", true);

//Basic
app.use(express.static(__dirname + "/public"));
app.get("/", (req, res) =>
  res.sendFile("test.html", { root: __dirname + "/views" })
);
app.get("/builder", (req, res) =>
  res.sendFile("builder.html", { root: __dirname + "/views" })
);

require("./app/builder.js")(app);
require("./sockets/index.js")(http);

http.listen(port, function() {
  console.log("listening on *:3000");
});
