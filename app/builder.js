var Char = require("../models/Char.js");
var uniqid = require("uniqid");

module.exports = function(app) {
  app.post("/save", (req, res) => {
    let data = req.body.char;
    // console.log("test", req.body.char);
    var json = JSON.stringify(req.body.char);
    if (data._id === "") {
      console.log(data.name);
      new Char({
        name: data.name,
        data: json,
        timestamp: Date.now()
      }).save(function(err, docs) {
        res.json({
          code: 200,
          id: docs.id
        });
      });
      return;
    }
    //DB
    Char.findOne(
      {
        _id: data._id
      },
      (err, docs) => {
        if (docs !== null) {
          docs.name = data.name;
          docs.data = json;
          docs.save();
          res.json({
            code: 200,
            id: docs.id
          });
        } else {
          new Char({
            name: data.name,
            data: json,
            timestamp: Date.now()
          }).save(function(err, docs) {
            res.json({
              code: 200,
              id: docs.id
            });
          });
        }
      }
    );
  });

  app.get("/char/all", (req, res) => {
    Char.find({}, (err, docs) => {
      let result = docs.map(x => {
        return {
          _id: x._id,
          name: x.name
        };
      });
      res.json(result);
    });
  });

  app.get("/char/update", (req, res) => {
    Char.find({}, (err, docs) => {
      let result = docs.map(x => {
        let data = JSON.parse(x.data);
        let skills = data.skills;
        // data.id = uniqid.time();
        skills.forEach(x => {
          // x.isMarking = [{ type: "default", value: false, schema: "bool" }];
          // x.id = uniqid.time();
          // x.effects.forEach(s => {
          //   s.id = uniqid.time();
          // });
          // console.log(x);
          // x.persistence = x.persistence[0].value;
          // x.class = x.class[0].value;
          // delete x.view;
        });
        // console.log(data);
        delete data.skillView;
        data.view = [];
        x.data = JSON.stringify(data);
        x.save();
        return {
          _id: x._id,
          name: x.name
        };
      });
      res.json(result);
    });
  });

  app.get("/char/get/:id", (req, res) => {
    console.log(req.params.id);
    Char.findOne({ _id: req.params.id }, (err, docs) => {
      res.json(docs);
    });
  });

  app.post("/char/delete/:id", (req, res) => {
    let data = req.body.char;
    Char.findOneAndDelete({ _id: data._id }, function(err, doc) {
      res.json({ code: 200 });
    });
  });
};
