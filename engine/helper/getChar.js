module.exports = function getSkill(pkg) {
  //Define
  let { team, charId, state } = pkg;
  //Return
  let charIndex = state[team].chars.findIndex(x => x.id === charId);
  return state[team].chars[charIndex];
};
