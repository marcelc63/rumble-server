const _ = require("lodash");
let evaluate = require("../parsers/evaluate.js");

function buff(pkg) {
  //Define
  let { val, efBuff, efDamage } = pkg;
  //Logic
  if (_.isObject(efBuff.scope)) {
    if (efBuff.scope.type === "skills") {
      if (
        efBuff.scope.detail === "is" &&
        !efBuff.scope.options.includes(efDamage.name)
      ) {
        return val;
      } else if (
        efBuff.scope.detail === "is not" &&
        efBuff.scope.options.includes(efDamage.name)
      ) {
        return val;
      }
    }
  }
  val = val + evaluate({ char, evaluatee: efBuff.val }, "int");
  //Return
  return val;
}

module.exports = buff;
