const _ = require("lodash");
let getChar = require("../helper/getChar.js");
let damageResolution = require("./operators/damageResolution");
let evaluate = require("../parsers/evaluate.js");

async function damage(pkg) {
  //Define
  let state = pkg.state;
  let { effect, char } = pkg;
  let caster = getChar({
    team: effect.caster.team,
    charId: effect.caster.charId,
    state
  });
  let target = char;
  let efDamage = effect;
  let val = evaluate(
    { state, char, evaluatee: effect.val, participant: effect.participant },
    "int"
  ); //can have parser here ltr
  //Logic
  val = await damageResolution({ state, caster, target, val, efDamage });
  target.hp = target.hp - val;
  //Can do side effects here ltr
  //Return
  return state;
}

module.exports = damage;
