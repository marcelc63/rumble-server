const _ = require("lodash");
let isDisable = require("../parsers/isDisable.js");

function dd(pkg) {
  //Define
  //   let state = pkg.state;
  let { val, efDd } = pkg;
  let valTemp = val;
  //Logic
  //Check Disable
  let disable = isDisable({ char, skill: efDd });
  if (disable) {
    return val; //Escape if ignore is true
  }
  val = val - efDd.current;
  efDd.current = Math.max(0, efDd.current - valTemp);
  val = Math.max(0, val);
  //Return
  return val;
}

module.exports = dd;
