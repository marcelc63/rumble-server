const _ = require("lodash");
let evaluate = require("../parsers/evaluate.js");
let energyCost = require("../energy/energyCost.js");

async function energy(pkg) {
  //Define
  let state = pkg.state;
  let { effect, char } = pkg;
  let team = effect.caster.team === "odd" ? "even" : "odd";
  let scope = effect.scope;
  let val = effect.val;
  let cost = {
    g: 0,
    r: 0,
    b: 0,
    w: 0,
    rd: 0
  };

  //Logic
  if (scope.type === "cost") {
    let options = scope.options;
    let details = scope.details;

    //If Random
    let choice = _.shuffle(["g", "r", "b", "w"])[0];
    if (options === "rd") {
      options = choice;
    }

    //Assign Cost
    if (options === "g") {
      cost.g = cost.g + val;
    } else if (options === "r") {
      cost.r = cost.r + val;
    } else if (options === "b") {
      cost.b = cost.b + val;
    } else if (options === "w") {
      cost.w = cost.w + val;
    }

    //Substract
    state[team].energy.g = Math.max(0, state[team].energy.g - cost.g);
    state[team].energy.r = Math.max(0, state[team].energy.r - cost.r);
    state[team].energy.b = Math.max(0, state[team].energy.b - cost.b);
    state[team].energy.w = Math.max(0, state[team].energy.w - cost.w);

    console.log(cost, state[team].energy);
    return state;
  }
  return state;
}

module.exports = energy;
