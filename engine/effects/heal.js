const _ = require("lodash");
let evaluate = require("../parsers/evaluate.js");

async function heal(pkg) {
  //Define
  let state = pkg.state;
  let { effect, char } = pkg;
  // let caster = getChar({
  //   team: effect.caster.team,
  //   charId: effect.caster.charId,
  //   state
  // });
  let target = char;
  let val = evaluate(
    { state, char, evaluatee: effect.val, participant: effect.participant },
    "int"
  ); //can have parser here ltr
  //Logic
  target.hp = target.hp + val;
  if (target.hp > target.maxHp) {
    target.hp = target.maxHp;
  }
  //Can do side effects here ltr
  //Return
  return state;
}

module.exports = heal;
