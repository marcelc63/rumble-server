const _ = require("lodash");
let buff = require("../buff.js");
let nerf = require("../nerf.js");
let boost = require("../boost.js");
let dr = require("../dr.js");
let dd = require("../dd.js");

async function damageResolution(pkg) {
  //Define
  let { caster, target, val, efDamage, state } = pkg;
  //Logic

  //Damage Manipulation from Caster
  //Buffs
  let getBuff = caster.status.onAttack.filter(x => x.type === "buff");
  for (efBuff of getBuff) {
    val = buff({ val, efBuff, efDamage, state });
  }
  let getNerf = caster.status.onAttack.filter(x => x.type === "nerf");
  for (efNerf of getNerf) {
    val = nerf({ val, efNerf, efDamage, state });
  }

  //Damage Manipulation from Receiver
  //Boost
  let getBoost = target.status.onReceive.filter(x => x.type === "boost");
  for (efBoost of getBoost) {
    console.log("BOOST COUNT");
    val = boost({ val, efBoost, efDamage, state });
  }

  //Damage Reduction
  let isPiercing = efDamage.isPiercing[0].value;
  console.log(isPiercing);
  if (!isPiercing) {
    //if Piercing, skip DR
    let getDr = target.status.onReceive.filter(x => x.type === "dr");
    for (efDr of getDr) {
      val = dr({ val, efDr, efDamage, state });
      console.log("PIERCING", val);
    }
  }

  //Damage Deduction
  let getDd = target.status.onReceive.filter(x => x.type === "dd");
  for (efDd of getDd) {
    if (efDamage.class !== "affliction") {
      val = dd({ val, efDd, efDamage, state });
      console.log("DD", val);
    }
  }

  //Return
  return val;
}

module.exports = damageResolution;
