const _ = require("lodash");
let evaluate = require("../parsers/evaluate.js");

function nerf(pkg) {
  //Define
  let { val, efNerf, efDamage } = pkg;
  //Logic
  if (_.isObject(efNerf.scope)) {
    if (efNerf.scope.type === "skills") {
      if (
        efNerf.scope.detail === "is" &&
        !efNerf.scope.options.includes(efDamage.name)
      ) {
        return val;
      } else if (
        efNerf.scope.detail === "is not" &&
        efNerf.scope.options.includes(efDamage.name)
      ) {
        return val;
      }
    }
    if (efNerf.scope.type === "classes") {
      if (
        efNerf.scope.detail === "inclusive" &&
        !efNerf.scope.options.includes(efDamage.class)
      ) {
        return val;
      } else if (
        efNerf.scope.detail === "exclusive" &&
        efNerf.scope.options.includes(efDamage.class)
      ) {
        return val;
      }
    }
  }
  let nerfVal = evaluate({ char, evaluatee: efNerf.val }, "int");
  let stack = 1;
  let isStack = efNerf.isStack[0].value;
  if (isStack) {
    stack = stack + efNerf.stack;
  }
  val = Math.max(0, val - nerfVal * stack);
  console.log("NERF", val);
  //Return
  return val;
}

module.exports = nerf;
