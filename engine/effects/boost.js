const _ = require("lodash");
let evaluate = require("../parsers/evaluate.js");

function boost(pkg) {
  //Define
  let { val, efBoost, efDamage } = pkg;
  //Logic
  if (_.isObject(efBoost.scope)) {
    if (efBoost.scope.type === "skills") {
      if (
        efBoost.scope.detail === "is" &&
        !efBoost.scope.options.includes(efDamage.name)
      ) {
        return val;
      } else if (
        efBoost.scope.detail === "is not" &&
        efBoost.scope.options.includes(efDamage.name)
      ) {
        return val;
      }
    }
  }
  val = val + evaluate({ char, evaluatee: efBoost.val }, "int");
  console.log("BOOST", val);
  //Return
  return val;
}

module.exports = boost;
