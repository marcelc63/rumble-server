let char = {
  _id: "5cbb39b1a3e3240ca8b5d7d8",
  name: "naruto",
  description: "",
  picture: "https://i.imgur.com/cC0JLiQ.jpg",
  anime: "Naruto",
  credit: {
    author: "",
    pictures: "",
    coder: ""
  },
  maxHp: 100,
  category: [],
  id: "naruto",
  status: {
    onAttack: [],
    onReceive: [],
    onSkill: [],
    onState: []
  },
  skills: [
    {
      name: "Uzumaki Combo",
      description: "",
      picture: "https://i.imgur.com/N7Icwsp.jpg",
      id: "",
      index: 0,
      caster: "",
      persistence: [
        {
          type: "default",
          value: "instant",
          schema: "string"
        }
      ],
      class: [
        {
          type: "default",
          value: "physical",
          schema: "string"
        }
      ],
      effects: [
        {
          name: "Uzumaki Combo",
          description: "",
          display: "",
          type: "damage",
          val: [
            {
              type: "default",
              value: "20",
              schema: "int"
            }
          ],
          action: "",
          target: [
            {
              type: "default",
              value: "target",
              schema: "string"
            }
          ],
          duration: [
            {
              type: "default",
              value: 1,
              schema: "int"
            }
          ],
          during: "this turn",
          after: [],
          condition: [],
          persistence: "",
          class: "physical",
          scope: {
            type: "all",
            options: [],
            detail: ""
          },
          id: "",
          caster: "",
          turnid: "",
          current: 0,
          usage: 0,
          stack: 0,
          charge: 0,
          multi: 0,
          active: true,
          isStack: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ],
          isInvisible: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ],
          isMulti: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ],
          isUnremovable: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ],
          isHarmful: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ],
          isPiercing: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ],
          isActive: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ],
          isLastTurn: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ]
        }
      ],
      target: [
        {
          type: "default",
          value: "enemy",
          schema: "string"
        }
      ],
      cooldown: [
        {
          type: "default",
          value: 0,
          schema: "int"
        }
      ],
      cost: {
        g: [
          {
            type: "default",
            value: "1",
            schema: "int"
          }
        ],
        r: [
          {
            type: "default",
            value: 0,
            schema: "int"
          }
        ],
        b: [
          {
            type: "default",
            value: 0,
            schema: "int"
          }
        ],
        w: [
          {
            type: "default",
            value: 0,
            schema: "int"
          }
        ],
        rd: [
          {
            type: "default",
            value: 0,
            schema: "int"
          }
        ]
      },
      counter: 0,
      active: true,
      store: [],
      isHarmful: [
        {
          type: "default",
          value: true,
          schema: "bool"
        }
      ],
      isAllowed: [
        {
          type: "default",
          value: true,
          schema: "bool"
        }
      ],
      isCooldown: false,
      isStore: [
        {
          type: "default",
          value: false,
          schema: "bool"
        }
      ],
      isIgnoreCounter: [
        {
          type: "default",
          value: false,
          schema: "bool"
        }
      ],
      isIgnoreStun: [
        {
          type: "default",
          value: false,
          schema: "bool"
        }
      ],
      isIgnoreInvul: [
        {
          type: "default",
          value: false,
          schema: "bool"
        }
      ]
    },
    {
      name: "Rasengan",
      description: "",
      picture: "https://i.imgur.com/K85sYdM.jpg",
      id: "",
      index: 0,
      caster: "",
      persistence: [
        {
          type: "default",
          value: "instant",
          schema: "string"
        }
      ],
      class: [
        {
          type: "default",
          value: "energy",
          schema: "string"
        }
      ],
      effects: [
        {
          name: "Rasengan",
          description: "",
          display: "",
          type: "stun",
          val: [
            {
              type: "default",
              value: 0,
              schema: "int"
            }
          ],
          action: "",
          target: [
            {
              type: "default",
              value: "target",
              schema: "string"
            }
          ],
          duration: [
            {
              type: "default",
              value: 1,
              schema: "int"
            }
          ],
          during: "this turn",
          after: [],
          condition: [],
          persistence: "",
          class: "mental",
          scope: {
            type: "all",
            options: [],
            detail: ""
          },
          id: "",
          caster: "",
          turnid: "",
          current: 0,
          usage: 0,
          stack: 0,
          charge: 0,
          multi: 0,
          active: true,
          isStack: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ],
          isInvisible: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ],
          isMulti: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ],
          isUnremovable: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ],
          isHarmful: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ],
          isPiercing: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ],
          isActive: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ],
          isLastTurn: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ]
        },
        {
          name: "",
          description: "",
          display: "",
          type: "damage",
          val: [
            {
              type: "default",
              value: "45",
              schema: "int"
            }
          ],
          action: "",
          target: [
            {
              type: "default",
              value: "target",
              schema: "string"
            }
          ],
          duration: [
            {
              type: "default",
              value: "1",
              schema: "int"
            }
          ],
          during: "this turn",
          after: [],
          condition: [],
          persistence: "",
          class: "mental",
          scope: {
            type: "all",
            options: [],
            detail: ""
          },
          id: "",
          caster: "",
          turnid: "",
          current: 0,
          usage: 0,
          stack: 0,
          charge: 0,
          multi: 0,
          active: true,
          isStack: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ],
          isInvisible: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ],
          isMulti: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ],
          isUnremovable: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ],
          isHarmful: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ],
          isPiercing: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ],
          isActive: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ],
          isLastTurn: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ]
        }
      ],
      target: [
        {
          type: "default",
          value: "enemy",
          schema: "string"
        }
      ],
      cooldown: [
        {
          type: "default",
          value: "1",
          schema: "int"
        }
      ],
      cost: {
        g: [
          {
            type: "default",
            value: 0,
            schema: "int"
          }
        ],
        r: [
          {
            type: "default",
            value: 0,
            schema: "int"
          }
        ],
        b: [
          {
            type: "default",
            value: "1",
            schema: "int"
          }
        ],
        w: [
          {
            type: "default",
            value: 0,
            schema: "int"
          }
        ],
        rd: [
          {
            type: "default",
            value: "1",
            schema: "int"
          }
        ]
      },
      counter: 0,
      active: true,
      store: [],
      isHarmful: [
        {
          type: "default",
          value: true,
          schema: "bool"
        }
      ],
      isAllowed: [
        {
          type: "default",
          value: true,
          schema: "bool"
        },
        {
          type: "condition",
          subject: {
            type: "active",
            owner: "effect",
            eval: "bool"
          },
          evaluator: "exist",
          comparison: "Shadow Clones",
          value: true
        }
      ],
      isCooldown: false,
      isStore: [
        {
          type: "default",
          value: false,
          schema: "bool"
        }
      ],
      isIgnoreCounter: [
        {
          type: "default",
          value: false,
          schema: "bool"
        }
      ],
      isIgnoreStun: [
        {
          type: "default",
          value: false,
          schema: "bool"
        }
      ],
      isIgnoreInvul: [
        {
          type: "default",
          value: false,
          schema: "bool"
        }
      ]
    },
    {
      name: "Shadow Clones",
      description: "",
      picture: "https://i.imgur.com/HgwONmv.jpg",
      id: "",
      index: 0,
      caster: "",
      persistence: [
        {
          type: "default",
          value: "instant",
          schema: "string"
        }
      ],
      class: [
        {
          type: "default",
          value: "energy",
          schema: "string"
        }
      ],
      effects: [
        {
          name: "",
          description: "",
          display: "",
          type: "allow",
          val: [
            {
              type: "default",
              value: 0,
              schema: "int"
            }
          ],
          action: "",
          target: [
            {
              type: "default",
              value: "target",
              schema: "string"
            }
          ],
          duration: [
            {
              type: "default",
              value: "4",
              schema: "int"
            }
          ],
          during: "next turn",
          after: [],
          condition: [],
          persistence: "",
          class: "energy",
          scope: {
            type: "all",
            options: [],
            detail: ""
          },
          id: "",
          caster: "",
          turnid: "",
          current: 0,
          usage: 0,
          stack: 0,
          charge: 0,
          multi: 0,
          active: true,
          isStack: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ],
          isInvisible: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ],
          isMulti: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ],
          isUnremovable: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ],
          isHarmful: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ],
          isPiercing: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ],
          isActive: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ],
          isLastTurn: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ]
        },
        {
          name: "",
          description: "",
          display: "",
          type: "buff",
          val: [
            {
              type: "default",
              value: "10",
              schema: "int"
            }
          ],
          action: "",
          target: [
            {
              type: "default",
              value: "target",
              schema: "string"
            }
          ],
          duration: [
            {
              type: "default",
              value: "4",
              schema: "int"
            }
          ],
          during: "this turn",
          after: [],
          condition: [],
          persistence: "",
          class: [
            {
              type: "default",
              value: "energy",
              schema: "string"
            }
          ],
          scope: {
            type: "skills",
            options: [2],
            detail: "is not"
          },
          id: "",
          caster: "",
          turnid: "",
          current: 0,
          usage: 0,
          stack: 0,
          charge: 0,
          multi: 0,
          active: true,
          isStack: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ],
          isInvisible: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ],
          isMulti: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ],
          isUnremovable: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ],
          isHarmful: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ],
          isPiercing: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ],
          isActive: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ],
          isLastTurn: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ]
        },
        {
          name: "",
          description: "",
          display: "",
          type: "dr",
          val: [
            {
              type: "default",
              value: "15",
              schema: "int"
            }
          ],
          action: "",
          target: [
            {
              type: "default",
              value: "target",
              schema: "string"
            }
          ],
          duration: [
            {
              type: "default",
              value: "4",
              schema: "int"
            }
          ],
          during: "this turn",
          after: [],
          condition: [],
          persistence: "",
          class: "energy",
          scope: {
            type: "all",
            options: [],
            detail: ""
          },
          id: "",
          caster: "",
          turnid: "",
          current: 0,
          usage: 0,
          stack: 0,
          charge: 0,
          multi: 0,
          active: true,
          isStack: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ],
          isInvisible: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ],
          isMulti: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ],
          isUnremovable: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ],
          isHarmful: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ],
          isPiercing: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ],
          isActive: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ],
          isLastTurn: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ]
        }
      ],
      target: [
        {
          type: "default",
          value: "self",
          schema: "string"
        }
      ],
      cooldown: [
        {
          type: "default",
          value: "3",
          schema: "int"
        }
      ],
      cost: {
        g: [
          {
            type: "default",
            value: 0,
            schema: "int"
          }
        ],
        r: [
          {
            type: "default",
            value: 0,
            schema: "int"
          }
        ],
        b: [
          {
            type: "default",
            value: 0,
            schema: "int"
          }
        ],
        w: [
          {
            type: "default",
            value: 0,
            schema: "int"
          }
        ],
        rd: [
          {
            type: "default",
            value: "1",
            schema: "int"
          }
        ]
      },
      counter: 0,
      active: true,
      store: [],
      isHarmful: [
        {
          type: "default",
          value: true,
          schema: "bool"
        }
      ],
      isAllowed: [
        {
          type: "default",
          value: true,
          schema: "bool"
        }
      ],
      isCooldown: false,
      isStore: [
        {
          type: "default",
          value: false,
          schema: "bool"
        }
      ],
      isIgnoreCounter: [
        {
          type: "default",
          value: false,
          schema: "bool"
        }
      ],
      isIgnoreStun: [
        {
          type: "default",
          value: false,
          schema: "bool"
        }
      ],
      isIgnoreInvul: [
        {
          type: "default",
          value: false,
          schema: "bool"
        }
      ]
    },
    {
      name: "Sexy Technique",
      description: "",
      picture: "https://i.imgur.com/SRh8P5d.jpg",
      id: "",
      index: 0,
      caster: "",
      persistence: [
        {
          type: "default",
          value: "instant",
          schema: "string"
        }
      ],
      class: [
        {
          type: "default",
          value: "strategic",
          schema: "string"
        }
      ],
      effects: [
        {
          name: "",
          description: "",
          display: "",
          type: "invul",
          val: [
            {
              type: "default",
              value: 0,
              schema: "int"
            }
          ],
          action: "",
          target: [
            {
              type: "default",
              value: "target",
              schema: "string"
            }
          ],
          duration: [
            {
              type: "default",
              value: 1,
              schema: "int"
            }
          ],
          during: "next turn",
          after: [],
          condition: [],
          persistence: "",
          class: [
            {
              type: "default",
              value: "strategic",
              schema: "string"
            }
          ],
          scope: {
            type: "all",
            options: [],
            detail: ""
          },
          id: "",
          caster: "",
          turnid: "",
          current: 0,
          usage: 0,
          stack: 0,
          charge: 0,
          multi: 0,
          active: true,
          isStack: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ],
          isInvisible: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ],
          isMulti: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ],
          isUnremovable: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ],
          isHarmful: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ],
          isPiercing: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ],
          isActive: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ],
          isLastTurn: [
            {
              type: "default",
              value: false,
              schema: "bool"
            }
          ]
        }
      ],
      target: [
        {
          type: "default",
          value: "self",
          schema: "string"
        }
      ],
      cooldown: [
        {
          type: "default",
          value: "4",
          schema: "int"
        }
      ],
      cost: {
        g: [
          {
            type: "default",
            value: 0,
            schema: "int"
          }
        ],
        r: [
          {
            type: "default",
            value: 0,
            schema: "int"
          }
        ],
        b: [
          {
            type: "default",
            value: 0,
            schema: "int"
          }
        ],
        w: [
          {
            type: "default",
            value: 0,
            schema: "int"
          }
        ],
        rd: [
          {
            type: "default",
            value: "1",
            schema: "int"
          }
        ]
      },
      counter: 0,
      active: true,
      store: [],
      isHarmful: [
        {
          type: "default",
          value: true,
          schema: "bool"
        }
      ],
      isAllowed: [
        {
          type: "default",
          value: true,
          schema: "bool"
        }
      ],
      isCooldown: false,
      isStore: [
        {
          type: "default",
          value: false,
          schema: "bool"
        }
      ],
      isIgnoreCounter: [
        {
          type: "default",
          value: false,
          schema: "bool"
        }
      ],
      isIgnoreStun: [
        {
          type: "default",
          value: false,
          schema: "bool"
        }
      ],
      isIgnoreInvul: [
        {
          type: "default",
          value: false,
          schema: "bool"
        }
      ]
    }
  ],
  hp: 100,
  alive: true
};

module.exports = char;
