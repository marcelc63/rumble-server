const _ = require("lodash");
let evaluate = require("../parsers/evaluate.js");

function effectAssign(pkg) {
  //Define
  let {
    status,
    char,
    effect,
    caster,
    target,
    turnid,
    picture,
    parent,
    persistence,
    name,
    state
  } = pkg;
  let thisTurn = caster.team;
  let nextTurn = caster.team === "odd" ? "even" : "odd";
  console.log(pkg);

  //Check Stack
  let isStack = effect.isStack[0].value;
  if (isStack) {
    let index = status.findIndex(x => x.type === "nerf" && x.name === name);
    if (index !== -1) {
      status[index].stack = status[index].stack + 1;
      return status;
    }
  }
  //For Evaluate
  let participant = {
    //Resolving target in evaluate
    caster,
    target
  };
  //Return
  let valType = effect.type === "transform" ? "string" : "int";
  let val = evaluate(
    {
      char,
      evaluatee: effect.val,
      participant,
      state
    },
    valType
  );
  
  return status.concat({
    ...effect,
    val,
    current: evaluate(
      {
        char,
        evaluatee: effect.val,
        participant,
        state
      },
      "int"
    ),
    duration: evaluate(
      {
        char,
        evaluatee: effect.duration,
        participant,
        state
      },
      "int"
    ),
    during: effect.during === "this turn" ? thisTurn : nextTurn,
    persistence,
    caster,
    participant,
    name,
    active: true,
    turnid,
    parent,
    picture
  });
}

module.exports = effectAssign;
