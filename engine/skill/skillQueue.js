const _ = require("lodash");
let getSkill = require("../helper/getSkill.js");
let getChar = require("../helper/getChar.js");
let skillTargeting = require("./skillTargeting.js");
let onSkillApply = require("./onSkillApply.js");
let energyCost = require("../energy/energyCost");
let skillCooldown = require("./skillCooldown");
let persistenceCheck = require("./persistenceCheck");
let persistenceCasterCheck = require("./persistenceCasterCheck");
let evaluate = require("../parsers/evaluate.js");
let evaluateCost = require("../parsers/cost.js");

async function skillQueue(pkg) {
  //Define
  let state = _.cloneDeep(pkg.state);
  let { ally, enemy, queue } = pkg;
  //Logic

  //Pre Sequence
  for (let { item, i } of queue
    .filter(x => x.turnid === state.turnid)
    .map((x, i) => {
      return { item: x, i: i };
    })) {
    //Get Skill
    let skill = getSkill({
      team: ally,
      charId: item.caster.charId,
      skillId: item.skillId,
      state
    });
    let char = getChar({ team: ally, charId: item.caster.charId, state });

    //Pay Energy Cost
    let cost = evaluateCost({ char, skill, state });
    state = await energyCost({ state, ally, cost });

    //Counter -> Later
    //Set Cooldown
    state = await skillCooldown({ state, item });

    //Target
    state = await skillTargeting({ state, char, ally, enemy, skill, item });
  }

  //Persistence Target State Check
  state = await persistenceCheck({ state, ally, enemy });

  //Sequence
  let allQueue = queue.concat(state[enemy].using);
  for (let { item, i } of allQueue.map((x, i) => {
    return { item: x, i: i };
  })) {
    //Persistence
    //Caster State Check
    let persistence = persistenceCasterCheck({ state, item });
    if (persistence) {
      continue;
    }
    //Apply Effect
    state = await onSkillApply({ state, ally, enemy, item });
  }

  //Post Sequence
  //Assign modified Queue to enemy
  state[enemy].using = allQueue.filter(x => x.caster.team === enemy);
  //Return
  return state;
}

module.exports = skillQueue;
