const _ = require("lodash");
let getChar = require("../helper/getChar.js");
let skillSort = require("./skillSort.js");
let evaluate = require("../parsers/evaluate.js");
let isInvul = require("../parsers/isInvul.js");

async function skillTargeting(pkg) {
  //Define
  let state = _.cloneDeep(pkg.state);
  let { char, skill, enemy, ally, item } = pkg;
  let { effects, picture } = skill;
  let { caster, target, turnid } = item;
  let parent = item.skillId;
  let targeting = evaluate({ char, evaluatee: skill.target });
  let persistence = skill.persistence;
  let name = skill.name;

  //Targeting
  if (targeting === "self and enemy") {
    let chars = [];
    if (caster.charId !== target.charId) {
      chars.push(getChar({ team: caster.team, charId: caster.charId, state }));
      chars.push(getChar({ team: target.team, charId: target.charId, state }));
    }
    if (caster.charId === target.charId) {
      chars.push(getChar({ team: target.team, charId: target.charId, state }));
    }
    for (let { char, charId, team } of chars.map((x, i) => {
      return {
        char: x,
        charId: x.id,
        team: x.team
      };
    })) {
      //Exclude Invulnerable Characters
      let invul = isInvul({ state, char, skill });
      if (invul && team === enemy) {
        continue;
      }
      //Assign Target
      let recipient = {
        charId: charId,
        team: team
      };
      //Sort
      state = await skillSort({
        ...item,
        recipient,
        picture,
        persistence,
        name,
        effects,
        parent,
        state
      });
    }
  } else if (targeting === "all") {
    let chars = [];
    if (target.team === enemy) {
      chars = state[enemy].chars.concat(state[ally].chars);
    }
    if (target.team === ally) {
      chars = state[ally].chars;
      console.log(chars, state[ally].chars);
    }
    console.log(chars, caster.team, target.team);
    for (let { char, charId, team } of chars.map((x, i) => {
      return {
        char: x,
        charId: x.id,
        team: x.team
      };
    })) {
      //Exclude Invulnerable Characters
      let invul = isInvul({ state, char, skill });
      if (invul && team === enemy) {
        continue;
      }
      //Assign Target
      let recipient = {
        charId: charId,
        team: team
      };
      //Sort
      state = await skillSort({
        ...item,
        recipient,
        picture,
        persistence,
        name,
        effects,
        parent,
        state
      });
    }
  } else if (targeting === "all enemies") {
    for (let { char, charId, team } of state[enemy].chars.map((x, i) => {
      return {
        char: x,
        charId: x.id,
        team: x.team
      };
    })) {
      //Exclude Invulnerable Characters
      let invul = isInvul({ state, char, skill });
      if (invul) {
        continue;
      }
      //Assign Target
      let recipient = {
        charId: charId,
        team: team
      };
      //Sort
      state = await skillSort({
        ...item,
        recipient,
        picture,
        persistence,
        name,
        effects,
        parent,
        state
      });
    }
  } else {
    console.log("single!");
    //Sort for Individual
    let recipient = {
      charId: target.charId,
      team: target.team
    };
    state = await skillSort({
      state,
      recipient,
      picture,
      persistence,
      name,
      effects,
      parent,
      ...item
    });
  }
  //Return
  return state;
}

module.exports = skillTargeting;
