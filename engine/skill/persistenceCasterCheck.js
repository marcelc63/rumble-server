let getSkill = require("../helper/getSkill.js");
let getChar = require("../helper/getChar.js");
let isStun = require("../parsers/isStun");

module.exports = function persistenceCheck(pkg) {
  //Define
  let { state, item } = pkg;
  let { caster, skillId } = item;
  // let char = state[caster.team].chars[caster.char];
  let char = getChar({ team: caster.team, charId: caster.charId, state });
  // let skill = char.skills[item.skill];
  let skill = getSkill({
    team: caster.team,
    charId: caster.charId,
    skillId,
    state
  });
  let persistence = skill.persistence;
  //Return
  if (persistence === "action") {
    let stun = isStun({ char, skill });
    if (stun) {
      return true;
    }
  }
  if (persistence === "control") {
    let stun = isStun({ char, skill });
    if (stun) {
      item.remove = true;
      return true;
    }
  }
  return false;
};
