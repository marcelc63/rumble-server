const _ = require("lodash");
let getChar = require("../helper/getChar.js");
let evaluate = require("../parsers/evaluate.js");
let assign = require("./effectAssign.js");

async function skillSort(pkg) {
  //Define
  let state = _.cloneDeep(pkg.state);
  let { caster, target, recipient, turnid, effects, index } = pkg;
  //Check
  if (state.turnid !== turnid) {
    return state;
  }
  //Logic
  for (effect of effects) {
    console.log("target", target);
    console.log("caster", caster);
    console.log("effect target", effect.target);

    //Effect Targeting, could be better but works for now
    let char = getChar({
      team: recipient.team,
      charId: recipient.charId,
      state
    });
    let targeting = evaluate({
      char,
      evaluatee: effect.target,
      participant: { caster, target: recipient },
      state
    });
    let status = char.status;

    //Check targeting
    if (targeting === "target") {
      if (!(char.id === target.charId)) {
        continue;
      }
    }
    if (targeting === "target team") {
      if (!(char.team === target.team)) {
        continue;
      }
    }
    if (targeting === "caster") {
      if (!(char.id === caster.charId)) {
        continue;
      }
    }
    if (targeting === "caster team") {
      if (!(char.team === caster.team)) {
        continue;
      }
    }
    if (targeting === "enemy") {
      if (char.id === caster.charId || char.team === caster.team) {
        continue;
      }
    }
    if (targeting === "enemy team") {
      if (char.team === caster.team) {
        continue;
      }
    }

    //Check condition
    let condition = evaluate({
      char,
      evaluatee: effect.condition,
      participant: { caster, target: recipient },
      state
    });
    console.log("condition", condition);
    if (!condition) {
      continue;
    }

    //Define
    let payload = {
      ...pkg,
      effect,
      char,
      state
    };
    if (effect.type === "damage") {
      status.onSkill = assign({
        status: status.onSkill,
        ...payload
      });
    }
    if (effect.type === "heal") {
      status.onSkill = assign({
        status: status.onSkill,
        ...payload
      });
    }
    if (effect.type === "energy") {
      status.onSkill = assign({
        status: status.onSkill,
        ...payload
      });
    }
    if (effect.type === "dr") {
      status.onReceive = assign({
        status: status.onReceive,
        ...payload
      });
    }
    if (effect.type === "dd") {
      status.onReceive = assign({
        status: status.onReceive,
        ...payload
      });
    }
    if (effect.type === "boost") {
      // let isStack = effect.isStack[0].value;
      status.onReceive = assign({
        status: status.onReceive,
        ...payload
      });
    }
    if (effect.type === "buff") {
      status.onAttack = assign({
        status: status.onAttack,
        ...payload
      });
    }
    if (effect.type === "nerf") {
      status.onAttack = assign({
        status: status.onAttack,
        ...payload
      });
    }
    if (effect.type === "allow") {
      status.onState = assign({
        status: status.onState,
        ...payload
      });
    }
    if (effect.type === "invul") {
      status.onState = assign({
        status: status.onState,
        ...payload
      });
    }
    if (effect.type === "stun") {
      status.onState = assign({
        status: status.onState,
        ...payload
      });
    }
    if (effect.type === "state") {
      status.onState = assign({
        status: status.onState,
        ...payload
      });
    }
    if (effect.type === "ignore") {
      status.onState = assign({
        status: status.onState,
        ...payload
      });
    }
    if (effect.type === "disable") {
      status.onState = assign({
        status: status.onState,
        ...payload
      });
    }
    if (effect.type === "mark") {
      status.onState = assign({
        status: status.onState,
        ...payload
      });
    }
    if (effect.type === "transform") {
      status.onSkill = assign({
        status: status.onSkill,
        ...payload
      });
    }
  }
  //Return
  return state;
}

module.exports = skillSort;
