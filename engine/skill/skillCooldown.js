const _ = require("lodash");
let getSkill = require("../helper/getSkill.js");

async function skillCooldown(pkg) {
  //Define
  let state = _.cloneDeep(pkg.state);
  let { caster, turnid, skillId } = pkg.item;
  let skill = getSkill({
    team: caster.team,
    charId: caster.charId,
    skillId: skillId,
    state
  });
  //Check
  if (state.turnid !== turnid) {
    return state;
  }
  //Logic
  skill.counter = skill.cooldown;
  //Return
  return state;
}

module.exports = skillCooldown;
