const _ = require("lodash");

function remove(status) {
  return status.filter(x => !(x.remove === true));
}

async function cleanupEffect(pkg) {
  //Define
  let state = _.cloneDeep(pkg.state);
  let { ally, enemy, queue } = pkg;
  let chars = state[ally].chars.concat(state[enemy].chars);
  //Logic
  for (char of chars) {
    char.status.onReceive = char.status.onReceive.map(x => {
      if (x.type === "dd" && x.current === 0) {
        return {
          ...x,
          remove: true
        };
      }
      return x;
    });
  }
  for (char of chars) {
    // char.status.onSkill = remove(char.status.onSkill, item.turnid);
    // char.status.onAttack = remove(char.status.onAttack, item.turnid);
    char.status.onReceive = remove(char.status.onReceive);
    // char.status.onState = remove(char.status.onState, item.turnid);
  }
  //Return
  return state;
}

module.exports = cleanupEffect;
