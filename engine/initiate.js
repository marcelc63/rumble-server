const _ = require("lodash");
// let getChar = require("./dummyChar.js");
// let newChar = require("./dummy/naruto.js");
// let shikamaru = require("./dummy/shikamaru.js");
// let sakura = require("./dummy/sakura.js");
let energy = require("./energy/energyGenerate");
let parser = require("./parsers/parser.js");
var Char = require("../models/Char.js");

function initiate(pkg, callback) {
  // let char = getChar();
  let players = pkg.players;
  var one = {};
  var two = {};
  var three = {};
  Char.find({}, (err, docs) => {
    one = JSON.parse(
      docs.filter(x => x._id == "5cc3ea925929a51d884786c6")[0].data
    );
    two = JSON.parse(
      docs.filter(x => x._id == "5cf3fb1143c2a18a6e4508d0")[0].data
    );
    three = JSON.parse(
      docs.filter(x => x._id == "5cd37e08384207ff7cd79e94")[0].data
    );

    //Inside
    function assignChars(chars, turn) {
      return chars.map(char => {
        let toAssign = _.cloneDeep(char);
        return {
          ...toAssign,
          team: turn,
          skills: char.skills.map(skill => {
            return {
              ...skill,
              effects: skill.effects.map(effect => {
                return {
                  ...effect,
                  id: effect.id
                };
              }),
              id: skill.id
            };
          }),
          id: toAssign.id + "-" + turn
        };
      });
    }

    let odd = {
      chars: assignChars([one, two, three], "odd"),
      energy: energy(1),
      name: players[0],
      using: []
    };
    let even = {
      chars: assignChars([one, two, three], "even"),
      energy: energy(),
      name: players[1],
      using: []
    };

    let stateBasic = {
      odd: odd,
      even: even,
      turn: 1,
      turnid: "turn1",
      timestamp: Date().now
    };

    let stateMeta = {
      room: pkg.room,
      channel: pkg.channel,
      log: []
    };

    let state = {
      ...stateBasic,
      ...stateMeta
    };

    //Exit
    let payload = { state };
    callback(payload);
  });
}

module.exports = initiate;
