const _ = require("lodash");
let evaluate = require("../parsers/evaluate.js");

async function durationReduce(pkg) {
  //Define
  let state = _.cloneDeep(pkg.state);
  let { ally, enemy, turnid, parent, caster } = pkg;
  let chars = state[ally].chars.concat(state[enemy].chars);
  let turn = state.turn % 2 === 0 ? "even" : "odd";
  //Check
  console.log(turnid, parent, caster, turn);
  //Logic
  for (char of chars) {
    let status = _.concat(
      char.status.onSkill,
      char.status.onReceive,
      char.status.onAttack,
      char.status.onState
    ).filter(
      x =>
        x.turnid === turnid &&
        x.parent === parent &&
        x.caster.charId === caster.charId &&
        x.caster.team === caster.team &&
        x.during === turn
    );
    for (effect of status) {
      console.log('checking effect')
      effect.duration = effect.duration - 1;
      // effect.current = evaluate({ char, evaluatee: effect.val }, "int");
      if (effect.type !== "dd") {
        effect.current = effect.val;
      } else {
        console.log("DD!", effect);
      }
      effect.active = true;
    }
  }
  //Return
  return state;
}

module.exports = durationReduce;
