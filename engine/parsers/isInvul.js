const _ = require("lodash");
let evaluate = require("./evaluate.js");
let isDisable = require("./isDisable.js");

function isInvul(pkg) {
  //Define
  let { char, skill } = pkg;
  //Logic
  let invul = char.status.onState.filter(x => x.type === "invul");
  if (invul.length > 0) {
    for (item of invul) {
      //Check Disable
      let disable = isDisable({
        char,
        skill: item
      });
      if (disable) {
        return false; //Escape if ignore is true
      }
      //Continue to evaluate Stun
      if (item.scope.type === "effect") {
        if (item.scope.detail === "inclusive") {
          return item.scope.options.some(x => x === skill.type);
        }
        if (item.scope.detail === "exclusive") {
          return item.scope.options.some(x => !(x === skill.type));
        }
      }
      if (item.scope.type === "classes") {
        if (item.scope.detail === "inclusive") {
          return item.scope.options.some(x => x === skill.class);
        }
        if (item.scope.detail === "exclusive") {
          return item.scope.options.some(x => !(x === skill.class));
        }
      }
      if (item.scope.type === "all") {
        return true;
      }
    }
  }
  //Return
  return false;
}

module.exports = isInvul;
