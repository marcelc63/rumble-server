const _ = require("lodash");
let evaluate = require("./evaluate.js");

//Parsers
let isAllowed = require("./isAllowed.js");
let isCooldown = require("./isCooldown.js");
let isMarking = require("./isMarking.js");
let isStun = require("./isStun");
let cost = require("./cost");

async function parser(pkg) {
  //Work to parsing stuns and default skill disable
  //Define
  let state = _.cloneDeep(pkg.state);
  let { ally, enemy } = pkg;
  let chars = state[ally].chars.concat(state[enemy].chars);

  //Skill Parsing
  for (let char of chars) {
    let skills = char.skills;

    for (let skill of skills) {
      let skillState = {
        isStun: false
      };
      //Information
      skill.target = evaluate({
        state,
        char,
        evaluatee: skill.target
      });
      //Parse Costs
      skill.cost = cost({ state, char, skill });
      //Parse Cooldown
      skill.cooldown = evaluate({
        state,
        char,
        evaluatee: skill.cooldown
      });
      //Parse isAllowed
      skill.isAllowed = isAllowed({ state, char, skill });
      //Parse Cooldown
      skill.isCooldown = isCooldown({ state, char, skill });
      //Parse Cooldown
      skill.isMarking = isMarking({ skill });
      //Check Stun
      skillState.isStun = isStun({ state, char, skill });
      //Assign Active
      if (
        skill.isAllowed === false ||
        skill.isCooldown === true ||
        skillState.isStun === true
      ) {
        skill.active = false;
      } else {
        skill.active = true;
      }
    }
  }

  //Return
  return state;
}

module.exports = parser;
