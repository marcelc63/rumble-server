const _ = require("lodash");
let evaluate = require("./evaluate.js");

function cost(pkg) {
  //Define
  let { char, skill, state } = pkg;
  let { g, r, b, w, rd } = skill.cost;
  //Logic
  //Return
  return {
    g: evaluate({ state, char, evaluatee: g }, "int"),
    r: evaluate({ state, char, evaluatee: r }, "int"),
    b: evaluate({ state, char, evaluatee: b }, "int"),
    w: evaluate({ state, char, evaluatee: w }, "int"),
    rd: evaluate({ state, char, evaluatee: rd }, "int")
  };
}

module.exports = cost;
