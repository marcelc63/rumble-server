const _ = require("lodash");
let evaluate = require("./evaluate.js");

function isIgnore(pkg) {
  //Define
  let { char, skill } = pkg;
  //Logic
  let ignore = char.status.onState.filter(x => x.type === "disable");
  if (ignore.length > 0) {
    for (item of ignore) {
      if (item.scope.type === "effects") {
        if (item.scope.detail === "inclusive") {
          return item.scope.options.some(x => x === skill.type);
        }
        if (item.scope.detail === "exclusive") {
          return item.scope.options.some(x => !(x === skill.type));
        }
      }
      if (item.scope.type === "classes") {
        if (item.scope.detail === "inclusive") {
          return item.scope.options.some(x => x === skill.class);
        }
        if (item.scope.detail === "exclusive") {
          return item.scope.options.some(x => !(x === skill.class));
        }
      }
      if (item.scope.type === "all") {
        return true;
      }
    }
  }
  //Return
  return false;
}

module.exports = isIgnore;
