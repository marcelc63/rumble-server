const _ = require("lodash");
let evaluate = require("./evaluate.js");

function isMarking(pkg) {
  //Define
  let { skill } = pkg;
  //Logic
  if (skill.isMarking.length > 1) {
    let marking = skill.isMarking;
    let evaluator = marking[1].evaluator;
    let comparison = marking[1].comparison;
    return [evaluator, comparison];
  }
  //Return
  return ["none"];
}

module.exports = isMarking;
